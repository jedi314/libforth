/** 
 \file throw.h
 \author: Giandomenico Rossi <g.rossi@onetastic.com>
 \copyright 2017 ONEtastic s.r.l.
 

 \brief try catch macros
*/

#ifndef _THROW_H_
#define _THROW_H_

#include <setjmp.h>

#define TRY(s) do { switch( setjmp(s) ) { case 0: 
#define CATCH(c) break; case c:
#define END_TRY break; } } while(0)
#define THROW(s,t) longjmp(s, t)

#endif 
