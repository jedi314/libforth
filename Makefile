CC=gcc
LD=gcc

PRG=forth
LIBS= -lreadline
CFLAGS= -g  -Wall -s -MMD -MP -MF "$@.d"
LDOPTIONS=
INCLUDE-DIRS=-I/usr/include \
	-I/usr/include/readline \
	-I./cpu -I.


SRC:=cli.c             \
	interpreter.c      \
	parser.c           \
	fqueue.c           \
	dictionary.c	   \
	terminal.c         \
	cpu/x86/forth.c
	#serial-terminal.c

OBJS:=$(SRC:%.c=%.o)



.PHONY: clean install all print

all: $(PRG)

$(PRG): $(OBJS)
	@echo -n  "LD $@: "
	$(LD) -o $@ $^ $(LIBS) $(LDOPTIONS)
	@echo "OK"


# lib: 

# Clean Targets
clean:
	@$(RM) $(OBJS) *~ $(addsuffix .d, $(OBJS))
	@$(RM) $(PRG)
	@echo "Removed obj files, and ~ .a"

print:
	@echo $(DEPS)
	@echo $(OBJS)


%.o: %.c
	@echo -n  "CC $<: "
	$(RM) "$@.d"
	$(CC) -c  $(CFLAGS) $(INCLUDE-DIRS) -o $@ $<
	@echo "OK"

# Dependencies
DEPS=$(wildcard $(addsuffix .d, $(OBJS)))

ifneq ($(DEPS),)
include $(DEPS)
endif

ifndef VERBOSE
.SILENT: 
endif
