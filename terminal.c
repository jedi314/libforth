/** ModularorT2
 \file terminal.c
 \author: Giandomenico Rossi <g.rossi@onetastic.com>
 \copyright 2017 ONEtastic s.r.l.
 

 \brief Higher level buffer handling function.
 Arrived charactes are checked by ACCEPT function and set inside Terminal Input Buffer (TIB)
 ACCEPT function checks if there are any, through new_key, and if so picks them up one by one, 

 through KEY, echoes or not it, checks for special chars like <BS> <CR> <LF>.
 ACCEPT has a loop and exits from its loop when it catch <CR> or <LF>
 Regular chars are copied to TIB buffer ready to be INTERPRET by the interpreter.

*/

#include "terminal.h"

#include <stdio.h>
/**
\brief outputs char to the current device.

\param term: pointer to SERIAL_TERMINAL struct
\param c: character to output
\return 0 if string is echoed 1 otherwise
*/
uint32_t emit(TERMINAL* term, char c)
{
    if (!term->echo_en)
        return 1;

    term->write(term->device, c);
    return 0;
}



/**
\brief sends a string.

\param term: pointer to SERIAL_TERMINAL struct
\param str:  pointer to string 
\param size: lentgth of string 
\return 0 if string is echoed 1 otherwise
*/
uint32_t type(TERMINAL* term, char* str, uint32_t size)
{
    uint32_t i;

    if (!term->echo_en)
        return 1;
    
    for(i=0; i<size && str[i] != 0 ; i++){
      emit(term, str[i]);
    }

    return 0;
}


static char get_format(uint32_t base)
{
    switch (base){
    case 8:
        return 'o';
    case 10:
        return 'd';
    case 16:
        return 'x';
    default:
        return 'd';
    }
    return 'd';
}

uint32_t type_number(TERMINAL* term, int32_t n)
{
    char number[12];
    uint32_t size;
    char format[3]="%d";
    
    format[1] = get_format(term->base);
    size = sprintf(number, format,  n);
    type(term, number, size);
    return 0;
}


void type_cr(TERMINAL* term)
{
    term->type(term, "\n", -1);
}

void type_lf(TERMINAL* term)
{
    term->type(term, "\r", -1);
}


void type_space(TERMINAL* term)
{
    term->type(term, " ", -1);
}

void type_ok(TERMINAL* term)
{
    term->type(term, "Ok", -1);
    type_cr(term);
}

uint32_t term_printf(TERMINAL* term, char* format, ... )
{
    va_list valist;
    char str[128];

    va_start(valist, format);
    vsprintf(str, format, valist);
    term->type(term, str, -1);
    return 0;
}

void type_error(TERMINAL* term, char* mag, uint32_t size)
{
    term->type(term, mag, size);
    term->type(term, " ?", -1);
    type_cr(term);
}



/**
\brief wrapper of emit function

\param term: pointer to term struct
\param c: character to echo
\return 
*/
void echo(TERMINAL* term, char c)
{
    emit(term, c);
}


/**
\brief Get at most 128 characters from the current input device, 
echo each, and place them in memory beginning of TIB buffer. 
The process continues until ACCEPT encounters a carriage return 
or line terminator. 
If the line terminator is not received before a count 128 
is reached, any excess characters are discarded. 
Return the actual count of characters

\param term: pointer to SERIAL_TERMINAL struct
\return number of char read;
*/
uint32_t accept(TERMINAL* term)
{
    char c;
    int16_t n_char;

    n_char = 0;
    while(1){
        c = key(term->queue);
        switch(c){
        case CR:
        case LF:
            return n_char;
        case BS:
            if(--n_char > 0  && n_char < TIB_SIZE ){
                echo(term, c);
                echo(term, BL);
                echo(term, c);
            }
            break;
        case DEL:
              if(--n_char > 0  && n_char < TIB_SIZE ){
                  echo(term, BS);
                  echo(term, BL);
                  echo(term, BS);
              }
              break;
        default:
            if(--n_char > 0  && n_char < TIB_SIZE ){
                echo(term,c);
                term->tib[n_char++] = c ;
            }else{
                echo(term, BS);
                echo(term, c);
                term->tib[n_char] = c ;
            }
        }
    }
}

