/* gcc  -o cli cli.c -lreadline */
/* arm-linux-gcc cli.c -lreadline -lncurses -o cli-arm */

#include <stdio.h>
#include <readline/readline.h>
#include <readline/history.h>
#include <stdlib.h>
#include <termios.h> /* POSIX terminal control definitions */
#include <sys/stat.h>
#include <fcntl.h>
#include <error.h>
#include <unistd.h>
#include <sys/ioctl.h>

typedef struct ep93xx_pin {
        unsigned char port;
        unsigned char num;
        signed char val;
} ep93xx_pin_t;

#define EP93XX_GPIO_IOCTL_BASE  'Z'
#define EP93XX_GPIO_PORTB 1
#define EP93XX_GPIO_INPUT               _IOW(EP93XX_GPIO_IOCTL_BASE, 0, ep93xx_pin_t)
#define EP93XX_GPIO_OUTPUT              _IOW(EP93XX_GPIO_IOCTL_BASE, 1, ep93xx_pin_t)
#define EP93XX_GPIO_SET                 _IOW(EP93XX_GPIO_IOCTL_BASE, 2, ep93xx_pin_t)
#define EP93XX_GPIO_CLEAR               _IOW(EP93XX_GPIO_IOCTL_BASE, 3, ep93xx_pin_t)
#define EP93XX_GPIO_READ                _IOR(EP93XX_GPIO_IOCTL_BASE, 4, ep93xx_pin_t)


char* device="/dev/ttyS1";

/**
\brief Open a serial port and configure it 
to starting  communication with oneAmplifier

\param dev: device name
\return handle, or -1 if an error occurred 
(in which case, errno is set appropriately).
*/
int open_serial_port(char* dev)
{
    struct termios options;
    int fd;

    /* OPEN SERIAL PORT */
    fd = open("/dev/ttyS1", O_RDWR  | O_NDELAY);
    if (fd == -1)
    {
        printf(" open_port: Unable to open /dev/ttyS1  ");
        return 0;
    }
    else
        fcntl(fd, F_SETFL, 0);

     /*Get the current options for the port...*/
    tcgetattr(fd, &options);

    options.c_cflag |= B115200;

    options.c_cflag &= ~CSTOPB; // 1 stop bit
    options.c_cflag &= ~PARENB; //parity disabled
    options.c_cflag &= ~CSIZE; //size masked
    options.c_cflag |= CS8;    //8 bit
    options.c_cflag |= CLOCAL | CREAD; //enable receiver & don't change owner
    options.c_cflag &= ~CRTSCTS;  //no flow control
    options.c_lflag &= ~(ICANON | ECHO | ECHOE | ISIG | XCASE); //raw mode

    options.c_iflag &= ~(INPCK | PARMRK | IGNPAR); //disable parity check & parity error report
    options.c_iflag &= ~ISTRIP;  //don't strip the 8th bit
    options.c_iflag &= ~(IUCLC | INLCR | ICRNL | IMAXBEL);    //disable uppercase to lower case conversion
    options.c_iflag &= ~(IXON | IXOFF | IXANY); //disable xon/off flow control

    options.c_oflag &= ~OPOST;

    options.c_cc[VMIN] = 0;
    options.c_cc[VTIME] = 20; //wait one second before report "timeout" error

    cfsetispeed(&options, B115200);
    cfsetospeed(&options, B115200);

    tcflush(fd, TCIFLUSH);

    /*Set the new options for the port...*/
    tcsetattr(fd, TCSANOW, &options);
    
    return fd;
}

/**
\brief Enable TX RS485 enable pin.
This mcu pin enable/disable the TX RS485 
enableo on RS485 transmitter.

\param device: GPIO MCU device 
\param enable: if true enable if false disable.
*/
void tx_enable(char* device, int enable)
{
    int fd, err_ioctl;
    ep93xx_pin_t pin;
    
    fd = open(device, O_RDWR | O_SYNC);
    if(fd)
    {
        pin.port = EP93XX_GPIO_PORTB;
        pin.num = 5;
        
        err_ioctl = ioctl(fd,EP93XX_GPIO_OUTPUT,&pin);

        if (!enable)
            err_ioctl = ioctl(fd, EP93XX_GPIO_CLEAR, &pin);
        else
            err_ioctl = ioctl(fd, EP93XX_GPIO_SET, &pin);

        close(fd);
    }
    else
    {
        printf("ERROR (ioctl)\n");
    }
}



/**
\brief Send readline buffer to oneamplifier on RS485 
bus

\param fd: serial handler
\param buff: readline buffer
\param len:size of readline buffer
\return 0 or a negative non-zero  number in case of error
*/
int write_line(int fd, char* buff, int len)
{
    int i, n; 
    char cr = '\n' ;
    for(i=0; i<len ;){
        if( (n = write(fd, &buff[i], len-i)) < 0){
            return -1;
        }
        i += n;
    }
    /* Send  */
    write (fd, &cr, 1);
    return 0;
}

/**
\brief Read data line from serial device.
It is waiting for an answer from target 
terminating with a linefeed '\r'

\param fd: serial handler
\param buff: buffer where store arrived data
\return n the # of bytes received 
*/
int read_line(int fd, char* buff)
{
    char c;
    int err; 
    int i = 0;
    
    err = 0;
    while( (err = read(fd, &c, 1)) > 0){
        if (c!='\r')
            buff[i++]=c;
        else
            break;
    }
    if(err<0)
        return err ;

    return i;
}


/**
\brief Attempt to send data to target then wait for an answer.

\param fd: serial device handler
\param buff: data buffer to send
\return number of chare read of -1 as error
*/
int  send_line(int fd, char* buff, char* answer)
{
    size_t size; 
    
    size = strlen(buff);
    if ( write_line(fd, buff, size)<0 ){
        perror("WRITE:");
        return -1;
    }
    if( ( size = read_line(fd, answer)) <0){
        perror("READ: ");
        return -1;
    }
    answer[size] = 0;
    return size;
}


int main(int argc, char ** argv)
{
    int fd;
    char *line, answer[255];
    size_t n;

    tx_enable("/dev/ep93xx-gpio", 1);

    if ((fd = open_serial_port(device)) < 1){
        perror("OPEN: ");
        return 1;
    }

    while(1)
    {
        line = readline("ONE> ");
        if(!line) break;
        if(*line) add_history(line);
        
        n = send_line(fd, line, answer);
        if (n>0){
            answer[n] = 0;
            printf("%s", answer);
        }
        tcflush(fd, TCIFLUSH);
        free(line);
    }
    return 0;
}
