/** ModularorT2
 \file interpreter.c
 \author: Giandomenico Rossi <g.rossi@onetastic.com>
 \copyright 2017 ONEtastic s.r.l.
 

 \brief
 Structure of interpreter loop.
 The interpreter has a
 The interpreter owns a dictionary containig all base words ( command )
 to be executed.
 It appempts to execute or convert to a number each word found
 in the current input buffer.  
 The token is returned by word and the following algorithm is applied:
 
 1. Search a token inside the dictionary.
 2. If found execute it.
 3. If not found, chek if it is a number then put it on stack
 4. Else return Error
*/
#include <string.h>
#include <stdlib.h>
#include <ctype.h>
#include "parser.h"

#include <stdio.h>


void reset_stack(INTERPRETER* interp)
{
    reset_queue(&interp->stack);
}


/**
\brief Is a number if string start with a number or with
#, +, -, $,%,&.
The leading char is removed, but + or -, and addr pointer and len are updated
\param addr: string 
\param len: length of string
\return a number representng the base or 0 if it is not a number
*/
int32_t is_number(char** addr, uint32_t *len)
{
    int8_t base = 0;

    switch( **addr ){
    case '#':
        base = 10;
        break;
    case '+':
    case '-':
        base = 10;
        return base;
    case '$':                   /* HEX format */
        base = 16;
        break;
    case '%':                   /* BINARY format */
        base = 2;
        break;
    case '&':                   /* OCTAL format */
        base = 8;
        break;
    default:
        if(isdigit(**addr))
            base = -1;
        return base;
    }
    (*addr)++;
    (*len)--;

    return base;
}



uint32_t reset_err(uint32_t err)
{
    return err > TOKEN_LEN_ZERO;  

}

/**
\brief
Attempt to convert a string at addr of length len into digits, using the
radix (e.g., 10 for decimal, 16 for hex) in BASE ant put it in number variable

\param addr: string address
\param len: length of address
\param base: numeric base
\param number: pointer tu number converted

\return if conversion is correct return 1 and value is stored in number return
0 otherwise.
*/
int32_t  convert_number(char* addr, uint32_t len, uint32_t base, int32_t *number)
{
    char* endp;
    int32_t n;

    n = strtol(addr, &endp, base);

    if (endp == addr + len  ){
        *number = n;
        return 1;
    }
    return 0;
}


/**
\brief 
Attempt to find a definition whose name is in a counted string at
addr word. 
FIND does a dictionary search of string in TIB.

\param dict: Dictionary where to seach string
\param word: pointer of string
\param len: len of string to search
\param xt: (output) filled with token address found.
\return If the definition is not found, return TOKEN_NOT_FOUND; if
the definition is found, return TOKEN_FOUND and its execution token (i.e. address in xt). 
*/
uint32_t find(DICTIONARY* dict, char* word, uint32_t len, DICTIONARY** xt)
{
    uint32_t length;
    DICTIONARY *entry;
    for (entry=dict; entry->name!=0 ; entry++){
        length = strlen(entry->name);
        if( length != len )
            continue;
        if(strncmp(entry->name, word, length) == 0){
            *xt = entry;
            return TOKEN_FOUND;
        }
    }
    return TOKEN_NOT_FOUND;
}


uint32_t execute_xt(INTERPRETER *interp, DICTIONARY *xt){
    uint32_t n_params;

    n_params = xt->params < 0 ? get_last(&interp->stack)+2:(uint32_t)xt->params;
    if (n_params > (uint32_t)interp->stack.in)
        THROW(interp->t, QUEUE_UNDERFLOW);
    return xt->token(&interp->stack);
}


void throw_token_not_found(INTERPRETER* interp, char* w, char* addr, uint32_t len)
{
    strncpy(w, addr, len);
    w[len]=0;
    THROW(interp->t, TOKEN_NOT_FOUND);
}

/**
\brief  INTERPRET attempts to execute or convert to a number 
each word found in the current input buffer.  

Throws -4 if the stack has underflowed as a result of executing a word.  
\param interp: pointer to INTERPRETER struct
\param w: in case of error poiunts the word not found
\return 0 if OK else a coded Error used by QUIT
*/
uint32_t interpret(INTERPRETER* interp, char* w)
{
    TERMINAL* term;
    char *addr;
    uint32_t len;
    uint32_t err;
    DICTIONARY* xt;
    int32_t base;
    int32_t number;
    QUEUE *stack = &interp->stack;

    term = interp->terminal;
    
    while(1){
        len = word(term, interp->separators, &addr );
        if (len == 0 )
            return TOKEN_LEN_ZERO;
        err = find(interp->dictionary, addr, len, &xt);
        if (err == TOKEN_FOUND){
            execute_xt(interp, xt);
            continue;
        } else{              /* check if number */
            if ( (base = is_number(&addr, &len)) ){
                base = base == -1 ? term->base: base;
                if ( convert_number(addr, len, base, &number) ){
                    if( stack->push(stack, number) ==  QUEUE_OVERFLOW )
                        THROW(interp->t, QUEUE_OVERFLOW) ;
                    err = TOKEN_FOUND;
                }else{
                    throw_token_not_found(interp, w,  addr, len);
                }
            }else{
                throw_token_not_found(interp, w,  addr, len);
            }
        }
    }
    return TOKEN_NOT_FOUND;
}



/**
\brief  QUIT is the high-level interpreter loop.

\param interp: pointer to INTERPRETER struct
\return 
*/
void quit(INTERPRETER *interp)
{
    TERMINAL* term;
    char name[64];

    term = interp->terminal;
    while(1){
        if (term->query)
            term->query(term);
        TRY(interp->t){
            interpret(interp, name);
            type_ok(term);
            type_lf(term);
            return ;
        }CATCH(QUEUE_UNDERFLOW){
            type(interp->terminal, "Stack Underflow", -1);
            reset_stack(interp);
        }CATCH(QUEUE_OVERFLOW){
            type(interp->terminal, "Stack Overflow", -1);
            reset_stack(interp);
        }CATCH(TOKEN_NOT_FOUND){
            term_printf(interp->terminal, "%s ?", name);
            reset_stack(interp);
        }
        END_TRY;
        type_cr(term);
        type_lf(term);
    }
}



