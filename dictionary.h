/** 
 \file dictionary.h
 \author: Giandomenico Rossi <g.rossi@onetastic.com>
 \copyright 2017 ONEtastic s.r.l.
 

 \brief Dictionary Declaration stuff
*/


#ifndef _DICTIONARY_H_
#define _DICTIONARY_H_

#include "fqueue.h"
#include <stdint.h>


typedef struct DICTIONARY{
    char*  name;
    int32_t params;
    uint32_t (*token)(QUEUE*);
}DICTIONARY;


typedef struct VOCABULARY{
    DICTIONARY *dictionary;
    struct VOCABULARY *next;
}VOCABULARY;



/* list of all core words as XMACRO */


#define XMACRO_INSTRUCTIONS                                          \
    X("GREET" , 0,  greet )                                          \
    X(".S"    , 0,  dump_stack )                                     \
    X("NIP"   , 2,  nip )                                            \
    X("+"     , 2,  plus  )                                          \
    X("-"     , 2,  minus  )                                         \
    X("*"     , 2,  star  )                                          \
    X("/"     , 2,  slash  )                                         \
    X("MOD"   , 2,  mod )                                            \
    X("/MOD"  , 2,  slash_mod )                                      \
    X("*/MOD" , 2,  star_slash_mode )                                \
    X("*/"    , 3,  star_slash )                                     \
    X("DROP"  , 1,  drop)                                            \
    X("."     , 1,  dot  )                                           \
    X("@"     , 1,  fetch)                                           \
    X("C@"    , 1,  cfetch)                                          \
    X("!"     , 2,  store)                                           \
    X("C!"    , 2,  cstore)                                          \
    X("U."    , 1,  udot)                                            \
    X("DEPTH" , 0,  depth)                                           \
    X("AND"   , 2,  and )                                            \
    X("OR"    , 2,  or)                                              \
    X("XOR"   , 2,  xor)                                             \
    X("INVERT", 1,  invert)                                          \
    X("LSHIFT", 2,  lshift)                                          \
    X("RSHIFT", 2,  rshift)                                          \
    X("SWAP"  , 2,  swap)                                            \
    X("DUP"   , 1,  dup)                                             \
    X("OVER"  , 2,  over)                                            \
    X("TUCK"  , 2,  tuck)                                            \
    X("PICK"  , -1, pick)                                            \
    X("ROT"   , 3,  rot)                                             \
    X("DUMP"  , 2,  dump)                                            \
    X("S"     , 0,  raw_dump_stack)                                  \
    X("/S"    , 0,  print_stack)                                     \
    X("H."    , 0,  hex_dot)                                         \
    X("?"     , 1,  question)                                        \
    X("ABS"   , 1,  absolute)                                        \
    X("MAX"   , 2,  maximum)                                         \
    X("MIN"   , 2,  minimum)                                         \
    X("NEGATE", 1,  negate)                                          \
    X("WITHIN", 3,  within)                                          \
    X("="     , 2,  equal)                                           \
    X("NOT"   , 1,  not)                                             \
    X("U<"    , 2, uless)                                            \
    X("U>"    , 2, umore)                                            \
    X("<"     , 2, less)                                             \
    X(">"     , 2, more)                                             \
    X("<>"    , 2, not_equal)                                        \
    X("0="    , 1, zero_equal)                                       \
    X("0<>"   , 1, zero_not_equal)                                   \
    X("0>"    , 1, zero_gt)                                          \



#define X(a,b,c) uint32_t c(QUEUE* q);
XMACRO_INSTRUCTIONS
#undef X



#endif
