/** 
 \file queue.h
 \author: Giandomenico Rossi <g.rossi@onetastic.com>
 \copyright 2017 ONEtastic s.r.l.
 

 \brief QUEUE handling
*/


#ifndef _QUEUE_H
#define _QUEUE_H

#define RDATA_SIZE 256
#define QUEUE_SIZE 128

#include "stdint.h"

/* Linear QUEUE */
typedef struct QUEUE {
    int32_t in;                      /**< Circular Receive queue input index */
    int32_t  data[RDATA_SIZE];       /**< Receive queue ring buffer */
    int32_t  (*pull)(struct QUEUE*);
    uint32_t (*push)(struct QUEUE*, int32_t);
    uint32_t (*get) (struct QUEUE* q, uint32_t n);
}QUEUE;


/* Circular QUEUE */
typedef struct RQUEUE {
    uint32_t rin;              /**< Circular Receive queue input index */
    uint32_t rout;             /**< Receive queue output index */
    char rdata[QUEUE_SIZE];    /**< Receive queue ring buffer */
    uint8_t  (*key)(struct RQUEUE* );
    uint32_t (*push)(struct RQUEUE*, char);
}RQUEUE;

typedef enum{
    QUEUE_OK = 0,
    QUEUE_OVERFLOW  = 4 ,
    QUEUE_UNDERFLOW = 5
}QUEUE_ERRORS;

uint8_t key(RQUEUE* q);
uint32_t new_key(RQUEUE* q);

void reset_queue(QUEUE* q);
int32_t pull(QUEUE* q);
uint32_t push(QUEUE* q, int32_t v );
uint32_t get_last(QUEUE* q);
uint32_t queue_get(QUEUE* q, uint32_t n);

#endif
