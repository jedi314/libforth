/** ModularorT2
 \file parser.c
 \author: Giandomenico Rossi <g.rossi@onetastic.com>
 \copyright 2017 ONEtastic s.r.l.
 

 \brief
 Parser looks for words in TIB, extrats them (ie. returns its  address and its lenght)
 The lasb word string is then passed to find (the parser core) which looks up its
 token if there is one.

*/

#include <stdint.h>
#include "terminal.h"

#include <stdio.h>


/**
\brief inputs a line of text into TIB and leaves its length in n_tib
\param term: pointer to terminal struct
\return 
*/
/*
void query(TERMINAL* term)
{
    term->n_tib = accept(term);
    term->in = 0;
}
*/

/**
\brief checks if character is not an element of 
sep array.

\param c: charcater to match
\param sep: separator array
\return true if it is not an element of sep
*/
uint32_t no_sep(char c, char* sep)
{
    uint32_t i;

    for (i=0; (int8_t)sep[i]!= -1; i++){
        if(sep[i] == c)
            return 0;
    }

    return 1;
}


/**
\brief  skips leading occurrences of char in the string, if any.  If
none, string is returned unchanged.

\param str: pointer to string
\param size: size of string
\param sep: array of delimter char 
\param addr: (output) the address of string found is set
\return the length of found string
*/
uint32_t skip(char* str, uint32_t size,  char* sep)
{
    uint32_t i ;

    for (i=0; i<size && !no_sep(str[i], sep); i++);
 
    return i;
}



/**
\brief  skips leading characters UNTIL it find char c in the string.  
If none, string is returned unchanged.

\param str: pointer to string
\param sep: array delimter char 
\param size: size of string
\param addr: (output) the address of string found is set
\return the length of found string
*/
uint32_t scan(char* str, uint32_t size, char* sep)
{
    uint32_t i ;

    for (i=0; i<size && no_sep(str[i], sep) ; i++);
    return i;
}



/**
\brief Parse text to the first instance of char c, returning the address and
length of a temporary location containing the parsed text
PARSE does not skip leading delimiters. If it encounters a leading
delimiter, it will simply report having found a zero-length string.

\param term: pointer to terminal struct
\parma sep: array of delimter char 
\param addr: (output) the address of string found is set
\return the length of found string
*/
uint32_t  parse(TERMINAL* term, char* sep)
{
    uint32_t in;

    in = term->in;
    term->in += scan(&term->tib[in], term->n_tib - in, sep);
    return term->in-in;
}



/**
\brief Skip any leading occurrences of the delimiter char. 
Parse text delimited by char. 
Return addr, the address of a temporary location containing 
the parsed text and a length of string. If the parse area is empty
or contains only the delimiter(s), the resulting string length is zero.

\param term: pointer to terminal struct
\param sep:  array of delimter char

\return the length of found string
*/
uint32_t word(TERMINAL* term, char* sep, char** addr)
{
    uint32_t pos;
    

    /* skip leading space */
    if ( (pos = skip(&term->tib[term->in], term->n_tib-term->in, sep)) >= TIB_SIZE)
        return 0;
    term->in += pos;
    
    /* parse until BL is found */
    pos=parse(term, sep);
    *addr = &term->tib[term->in-pos];
    return pos;
    
}

