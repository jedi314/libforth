/** 
 \file cmd.c
 \author: Giandomenico Rossi <g.rossi@onetastic.com>
 \copyright 2017 ONEtastic s.r.l.
 

 \brief All Data for shared memory inquired by Drive 
*/

#include "main.h"
#include "interpreter.h"
#include "cmd.h"

#include <stddef.h>

typedef MULTITASTICEXTAMPINFO INFO_AMP;
void TurnOnOffAmplifier(_Bool on);
void reset_ampl(void);
void reset_alarm2( void );

typedef  MULTITASTICEXTAMPCONFIG amp_config;
typedef  MULTITASTIC_MODULATOR_CONFIG_SOFTWARE_OPTIONS SW_OPT;
typedef  MULTITASTIC_MODULATOR_CONFIG_HARDWARE_OPTIONS HW_OPT;
typedef  MULTITASTIC_MODULATOR_CONFIG_OPTIONS_STATUS   SW_OPT_STATUS;

typedef enum {
    VHF1 = 1,
    VHF3,
    UHF
}BAND;

const char* mode_list[]={"DVBT", "DVBT2", "ISDBT", "REPEATER", "ANALOG", "DAB", "ATSC" };
const char* band_list[]={"UNKNOWN", "VHF1", "VHF3", "UHF"};

#define N_PSU 1

uint32_t info(QUEUE *q)
{
    INFO_AMP info;

    ShMem_Copy_AmpInfo(&info);
    q->push(q, info.Model);  
    return 0;
}


uint32_t dot_info(QUEUE *q)
{
    INTERPRETER* interp = (INTERPRETER*)q;
    TERMINAL* term = interp->terminal;

    info(q);
    term_printf(term, "Model: %d\n", q->pull(q));
    term_printf(term, "Release: %s\n", g_shmem.AmpInfo.FirmwareRelease );

    return 0;  

}

/* AMP STATUS */

#define X(a,b,c)                                                \
    uint32_t c(QUEUE *q){                                       \
        q->push(q, (uint32_t)&g_shmem.AmpStatus.Amp.c);         \
        return 0;                                               \
    }
XAMP_STATUS
#undef X



uint32_t dump_struct(QUEUE* q, const char** s, uint32_t n)
{
    uint32_t i;
    int32_t v;
    INTERPRETER* interp = (INTERPRETER*)q;
    TERMINAL* term = interp->terminal;
    
    for(i=0; (uint32_t)i<n; i++){
        v = q->pull(q);
        term_printf(term, "%s: %d\n", s[n-i-1], v); 
    }

    return 0;
}


uint32_t amp_get(QUEUE *q)
{
#define X(a,b,c) g_shmem.AmpStatus.Amp.c,
/* Amp Status */
    const int32_t amp_status[] = {
        XAMP_STATUS
#undef X
        -1
    };  
    uint32_t i;
    
    for(i=0; amp_status[i] != -1; i++){
        q->push(q, amp_status[i]);
    }
    
    return 0;
}

uint32_t dot_amp(QUEUE* q)
{
    uint32_t n;

#define X(a,b,c) a,
    const char* fields[]={
        XAMP_STATUS
    };
#undef X
    n = sizeof(fields)/sizeof(fields[0]);
    
    amp_get(q);
    return dump_struct(q, fields, n );
}




/* PSU STATUS */

#define X(a,b,c)                                                \
    uint32_t c(QUEUE *q){                                       \
        q->push(q, offsetof( MULTITASTICEXTAMPPSUSTATUS, c));   \
        return 0;                                               \
    }
XPSU_STATUS
#undef X

uint32_t get_psu(QUEUE *q)
{
    uint32_t i, offset;
    
    i = q->pull(q);
    offset = q->pull(q);
    q->push(q, *((int32_t*)(((uint8_t*)&g_shmem.AmpStatus.Psu[i])+offset)));
    return 0;
}

uint32_t psu_get(QUEUE *q)
{

    uint32_t psu = q->pull(q);
    
#define X(a,b,c) g_shmem.AmpStatus.Psu[psu].c,
/* PSU Status */
    const int32_t psu_status[] = {
        XPSU_STATUS
#undef X
        -1
    };  
    uint32_t i;
    
    for(i=0; psu_status[i] != -1; i++){
        q->push(q, psu_status[i]);
    }
    
    return 0;
}

uint32_t zero_psu_get(QUEUE* q)
{
    q->push(q, 0);
    psu_get(q);

    return 0;
}

uint32_t dot_psu(QUEUE* q)
{
    uint32_t n;

#define X(a,b,c) a,
    const char* fields[]={
        XPSU_STATUS
    };
#undef X
    n = sizeof(fields)/sizeof(fields[0]);

    psu_get(q);
    return dump_struct(q, fields, n );
}

uint32_t zero_dot_psu(QUEUE* q)
{
    q->push(q, 0);
    dot_psu(q);

    return 0;
}


uint32_t opto_get(QUEUE *q)
{
    q->push(q, g_shmem.AmpStatus.Opto[q->pull(q)].Value);
    return 0;
}

uint32_t relay_get(QUEUE *q)
{
    q->push(q, g_shmem.AmpStatus.Relays[q->pull(q)].Value);
    return 0;
}

uint32_t zero_opto_get(QUEUE *q)
{
    q->push(q, g_shmem.AmpStatus.Opto[0].Value);
    return 0;
}

uint32_t zero_relay_get(QUEUE *q)
{
    q->push(q, g_shmem.AmpStatus.Relays[0].Value);
    return 0;
}



/* AMP Alarms */


#define X(a,b,c)                                                \
    uint32_t c##_alarm(QUEUE *q){                               \
        q->push(q, (uint32_t)&g_shmem.AmpAlarm.Amp.c);          \
        return 0;                                               \
    }
XAMP_ALARM
#undef X

uint32_t alarm_get(QUEUE *q)
{
#define X(a,b,c) g_shmem.AmpAlarm.Amp.c,
/* Amp Status */
    const int32_t amp_alarm[] = {
        XAMP_ALARM
#undef X
    };  
    uint32_t i, n;
    n = sizeof(amp_alarm)/sizeof(amp_alarm[0]);
    
    for(i=0; i<n; i++){
        q->push(q, amp_alarm[i]);
    }
    
    return 0;
}

uint32_t dot_alarm_get(QUEUE* q)
{
    uint32_t n;

#define X(a,b,c) a,
    const char* fields[]={
        XAMP_ALARM
    };
#undef X
    n = sizeof(fields)/sizeof(fields[0]);
    
    alarm_get(q);
    return dump_struct(q, fields, n );
}




/* PSU ALARMS */

uint32_t psu_alarm_get(QUEUE *q)
{
    uint32_t psu = q->pull(q);
#define X(a,b,c) g_shmem.AmpAlarm.Psu[psu].c,
    const int32_t psu_alarm[] = {
        XPSU_ALARM
#undef X
        -1
    };  
    uint32_t i;
    
    for(i=0; psu_alarm[i] != -1; i++){
        q->push(q, psu_alarm[i]);
    }
    
    return 0;
}

uint32_t dot_psu_alarm_get(QUEUE* q)
{
    uint32_t n;

#define X(a,b,c) a,
    const char* fields[]={
        XPSU_ALARM
    };
#undef X
    n = sizeof(fields)/sizeof(fields[0]);
    
    psu_alarm_get(q);
    return dump_struct(q, fields, n );
}


uint32_t all_alarm_get(QUEUE *q)
{
    uint32_t i;
    alarm_get(q);

    for(i=0; i< N_PSU; i++){
        q->push(q, i);
        psu_alarm_get(q);
    }
    
    return 0;
}


uint32_t dot_all_alarm_get(QUEUE *q)
{
    uint32_t i;
    dot_alarm_get(q);

    for(i=0; i< N_PSU; i++){
        q->push(q, i);
        dot_psu_alarm_get(q);
    }
    
    return 0;
}



/* CONFIG */


uint32_t multiset(QUEUE *q, int32_t *p, uint32_t n)
{
    uint32_t i;
    for (i=0; i<n; i++){
        *p++ = q->pull(q);
    }
    return 0;
}


uint32_t multiget(QUEUE *q, int32_t *p, uint32_t n)
{
    uint32_t i;
    for (i=0; i<n; i++){
        q->push(q, *(p+n-i-1));
    }
    return 0;
}


uint32_t multicset(QUEUE *q, int8_t *p, uint32_t n)
{
    uint32_t i;
    for (i=0; i<n; i++){
        *p++ = q->pull(q);
    }
    return 0;
}


uint32_t multicget(QUEUE *q, int8_t *p, uint32_t n)
{
    uint32_t i;
    for (i=0; i<n; i++){
        q->push(q, *(p+n-i-1));
    }
    return 0;
}


uint32_t reset(QUEUE *q)
{
    g_shmem.Config.ext_power_amplifier.Amp.RfEnable = TruthValue_true;
    TurnOnOffAmplifier(TRUE);    
    return 0;
}



uint32_t turn_on(QUEUE *q)
{
    g_shmem.Config.ext_power_amplifier.Amp.RfEnable = TruthValue_true;
    TurnOnOffAmplifier(TRUE);    
    return 0;
}

uint32_t turn_off(QUEUE *q)
{
    g_shmem.Config.ext_power_amplifier.Amp.RfEnable = TruthValue_false;
    TurnOnOffAmplifier(FALSE);    
    return 0;
}


uint32_t amp_th_set(QUEUE *q)
{
    int32_t *p = (int32_t*)&g_shmem.Config.ext_power_amplifier.Amp.Threshold;
    multiset(q, p, 3);
    return 0;
}

uint32_t amp_th_get(QUEUE *q)
{
    int32_t *p = (int32_t*)&g_shmem.Config.ext_power_amplifier.Amp.Threshold;
    multiget(q, p, 3);
    return 0;
}

uint32_t voltage_th_set(QUEUE *q)
{
    int32_t *p = (int32_t*)&g_shmem.Config.ext_power_amplifier.Psu.Threshold +  2 * q->pull(q);
    multiset(q, p, 2);
    return 0;
}


uint32_t voltage_th_get(QUEUE *q)
{
    int32_t *p = (int32_t*)&g_shmem.Config.ext_power_amplifier.Psu.Threshold + 2 * q->pull(q);
    multiget(q, p, 2);
    return 0;
}

uint32_t current_th_set(QUEUE *q)
{
    g_shmem.Config.ext_power_amplifier.Psu.Threshold.Current = q->pull(q);
    return 0;
}
                                            
uint32_t current_th_get(QUEUE *q)
{
    q->push(q, g_shmem.Config.ext_power_amplifier.Psu.Threshold.Current);
    return 0;
}

uint32_t psu_config_alarm_set(QUEUE *q)
{
    int8_t *p = (int8_t*)&g_shmem.Config.ext_power_amplifier.Psu.AlarmEnable;
    multicset(q, p, 3);
    return 0;
}

uint32_t psu_config_alarm_get(QUEUE *q)
{
    int8_t *p = (int8_t*)&g_shmem.Config.ext_power_amplifier.Psu.AlarmEnable;
    multicget(q, p, 3);
    return 0;
}


uint32_t amp_config_alarm_set(QUEUE *q)
{
    int8_t *p = (int8_t*)&g_shmem.Config.ext_power_amplifier.Amp.AlarmEnable;
    multicset(q, p, 5);
    return 0;
}

uint32_t amp_config_alarm_get(QUEUE *q)
{
    int8_t *p = (int8_t*)&g_shmem.Config.ext_power_amplifier.Amp.AlarmEnable;
    multicget(q, p, 5);
    return 0;
}

uint32_t amp_offset_set(QUEUE *q)
{
    int32_t *p = (int32_t*)&g_shmem.Config.ext_power_amplifier.Amp.Offset;
    multiset(q, p, 3);
    return 0;
}


uint32_t amp_offset_get(QUEUE *q)
{
    int32_t *p = (int32_t*)&g_shmem.Config.ext_power_amplifier.Amp.Offset;
    multiget(q, p, 3);
    return 0;
}


uint32_t save_amp_config(QUEUE *q)
{
    WriteBlockNvm(I2C1,
                  AMPLIFIER_EEPROM_ADDR,
                  (unsigned char*)&g_shmem.Config.ext_power_amplifier,
                  EEPROM_EXT_AMP_SIZE,
                  EEPROM_EXT_AMP_ADDR,
                  AMPLIFIER_EEPROM_PAGE_SIZE);
    
    return 0;
}

uint32_t load_amp_config(QUEUE *q)
{
    GetBlockNvm(I2C1,
                AMPLIFIER_EEPROM_ADDR,
                (unsigned char*)&g_shmem.Config.ext_power_amplifier,
                EEPROM_EXT_AMP_SIZE,
                EEPROM_EXT_AMP_ADDR);
    
      return 0;
}


uint32_t reset_alarms(QUEUE *q)
{
    reset_alarm2();
    return 0;
}

uint32_t set_channel(QUEUE *q)
{
    g_shmem.Config.modulator.UpConverter.ChannelNumber = q->pull(q);
    return 0;
}

uint32_t get_channel(QUEUE *q)
{
    q->push(q, g_shmem.Config.modulator.UpConverter.ChannelNumber);
    return 0;
}


uint32_t set_band(QUEUE *q)
{
    uint32_t band = q->pull(q);
    HW_OPT *hw = &g_shmem.Config.modulator.HwOptions;

    hw->Band  = band;
    switch (band){
    case VHF1:
        hw->Band1 = TruthValue_true;
        hw->Vhf   = TruthValue_false;
        break;
    case VHF3:
        hw->Band1 = TruthValue_false;
        hw->Vhf   = TruthValue_true;
        break;
    case UHF:
        hw->Band1 = TruthValue_false;
        hw->Vhf   = TruthValue_false;
        break;
    default:
        hw->Band1 = TruthValue_false;
        hw->Vhf   = TruthValue_false;
        break;
    }
    return 0;
}
uint32_t get_band(QUEUE *q)
{
    q->push(q, g_shmem.Config.modulator.HwOptions.Band);
    return 0;
}

uint32_t get_mode(QUEUE *q)
{
    SW_OPT_STATUS *sw_org = (SW_OPT_STATUS*)&g_shmem.Config.modulator.SwOptions;
    SW_OPT_STATUS *sw = sw_org;

    get_band(q);
    while((uint8_t*)sw < (uint8_t*)sw_org + sizeof(SW_OPT)){
        if (sw->Used == TruthValue_true){
            q->push (q, sw-sw_org);
            break;
        }
        sw++;
    }
 
   return 0;
}

uint32_t set_mode(QUEUE *q)
{
    uint32_t mode, old_mode;
    
    get_mode(q);
    old_mode = q->pull(q);
    drop(q);
    mode = q->pull(q); 

    SW_OPT_STATUS *sw = (SW_OPT_STATUS*)&g_shmem.Config.modulator.SwOptions;
    (sw+old_mode)->Used = TruthValue_false;
    (sw+mode)->Used = TruthValue_true;
    set_band(q);

    return 0;
}

uint32_t dot_mode(QUEUE* q)
{
    INTERPRETER* interp = (INTERPRETER*)q;
    TERMINAL* term = interp->terminal;
    get_mode(q);

    term_printf(term, "%s %s\n", mode_list[q->pull(q)], band_list[q->pull(q)]);
    return 0;
}

#define X(a,b,c)                                                    \
    uint32_t c##_alarm_config(QUEUE *q){                            \
        q->push(q, (uint32_t)&g_shmem.Config.ext_power_amplifier.Amp.AlarmEnable.c); \
        return 0;                                                   \
    }
XAMP_ALARM_CONFIG
#undef X






