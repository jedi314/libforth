/** 
 \file cmd.h
 \author: Giandomenico Rossi <g.rossi@onetastic.com>
 \copyright 2017 ONEtastic s.r.l.
 

 \brief
*/


#ifndef _CMD_H
#define _CMD_H

#include "fqueue.h"

uint32_t info(QUEUE *q);
uint32_t dot_info(QUEUE *q);

#define XAMP_STATUS                                 \
    X("Rf", 0, Rf)                                  \
    X("ForwardPower", 0, ForwardPower)              \
    X("ForwardPowerdBm", 0, ForwardPowerdBm)        \
    X("ReflectedPower", 0, ReflectedPower)          \
    X("ReflectedPowerdBm", 0, ReflectedPowerdBm)    \
    X("Temperature1", 0, Temperature1)              \
    X("Temperature2", 0, Temperature2)              \
    X("DriverInUse", 0, DriverInUse)                \
    X("Fan1", 0, Fan1)                              \
    X("Fan2", 0, Fan2)                              \
    X("Fan3", 0, Fan3)                              \
    
//X("Fan4", 0, Fan4)                            

#define XPSU_STATUS                             \
    X("Voltage1", 0, Voltage1)                  \
    X("Voltage2", 0, Voltage2)                  \
    X("Current1", 0, Current1)                  \

    //X("Current2", 0, Current2)                
    //X("Current3", 0, Current3)                
    //X("Current4", 0, Current4)                

#define XOPTO_STATUS                            \
    X("OPTO@", 1, opto_get)                     \
    X("0OPTO@", 0, zero_opto_get)               \
    
#define XRELAY_STATUS                           \
    X("RELAY@", 1, relay_get)                   \
    X("0RELAY@", 0, zero_relay_get)              \
    
#define XAMP_ALARM                               \
    X("aForwardHigh", 0, ForwardHigh)            \
    X("aForwardLow", 0, ForwardLow)              \
    X("aReflected", 0, Reflected)                \
    X("aTemperature", 0, Temperature)            \
    X("aFan1", 0, Fan1)                          \
    X("aFan2", 0, Fan2)                          \
    X("aFan3", 0, Fan3)                          \
    /* X("aFan4", 0, Fan4)                         \ */
    /* X("OptoStandBy", 0, OptoStandBy)            \ */
    /* X("LoadInterlock", 0, LoadInterlock)        \ */
    /* X("CommErrorDriver1", 0, CommErrorDriver1)  \ */
    /* X("CommErrorDriver2", 0, CommErrorDriver2)  \ */
    /* X("OutOfRange", 0, OutOfRange)              \ */

#define XPSU_ALARM                               \
    X("aVoltage1", 0, Voltage1)                  \
    X("aVoltage2", 0, Voltage2)                  \
    X("aCurrent", 0, Current)                    \

#define XCMD                                                         \
    X("INFO"  , 0,  info)                                            \
    X(".INFO" , 0,  dot_info)                                        \
    X("AMP@",  0, amp_get)                                           \
    X(".AMP",  0, dot_amp)                                           \
    X("@PSU",  2, get_psu)                                           \
    X("PSU@",  1, psu_get)                                           \
    X("0PSU@", 0, zero_psu_get)                                      \
    X(".PSU",  1, dot_psu)                                           \
    X("0.PSU", 0, zero_dot_psu)                                      \
    X("PSU-ALARM@",  1, psu_alarm_get)                               \
    X("ALARM@",  0, alarm_get)                                       \
    X("ALL-ALARMS@",  0, all_alarm_get)                              \
    X(".PSU-ALARM",  1, dot_psu_alarm_get)                           \
    X(".ALARM",  0, dot_alarm_get)                                   \
    X(".ALL-ALARMS",  0, dot_all_alarm_get)                          \
    X("RESET", 0, reset)                                             \
    X("TURN-ON", 0, turn_on)                                         \
    X("TURN-OFF", 0, turn_off)                                       \
    X("AMP-TH!", 3, amp_th_set)                                      \
    X("AMP-TH@", 0, amp_th_get)                                      \
    X("PSU-VOLTAGE-TH!", 3, voltage_th_set)                          \
    X("PSU-CURRENT-TH!", 1, current_th_set)                          \
    X("PSU-VOLTAGE-TH@", 1, voltage_th_get)                          \
    X("PSU-CURRENT-TH@", 0, current_th_get)                          \
    X("PSU-ALARM-CONF!", 3, psu_config_alarm_set)                    \
    X("AMP-ALARM-CONF!", 5, amp_config_alarm_set)                    \
    X("PSU-ALARM-CONF@", 0, psu_config_alarm_get)                    \
    X("AMP-ALARM-CONF@", 0, amp_config_alarm_get)                    \
    X("AMP-OFFSET!", 2, amp_offset_set)                              \
    X("AMP-OFFSET@", 0, amp_offset_get)                              \
    X("SAVE-CONFIG", 0, save_amp_config)                             \
    X("LOAD-CONFIG", 0, load_amp_config)                             \
    X("RESET-ALARMS", 0, reset_alarms)                               \
    X("CHANNEL!", 1, set_channel)                                    \
    X("CHANNEL@", 0, get_channel)                                    \
    X("MODE!", 2, set_mode)                                          \
    X("MODE@", 0, get_mode)                                          \
    X(".MODE", 0, dot_mode)                                          \

#define XAMP_ALARM_CONFIG                       \
    X("ForwardHigh", 0, ForwardHigh)            \
    X("ForwardLow", 0, ForwardLow)              \
    X("Reflected", 0, Reflected)                \
    X("Temperature", 0, Temperature)            \
    X("Fan", 0, Fan)                            \
    
#define X(a,b,c) uint32_t c(QUEUE* q);
XAMP_STATUS
XCMD
XPSU_STATUS
XOPTO_STATUS
XRELAY_STATUS
#undef X
#define X(a,b,c) uint32_t c##_alarm(QUEUE* q);
XAMP_ALARM
#undef X
#define X(a,b,c) uint32_t c##_alarm_config(QUEUE* q);
XAMP_ALARM_CONFIG
#undef X

#endif
