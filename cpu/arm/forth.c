/** 
 \file forth.c
 \author: Giandomenico Rossi <g.rossi@onetastic.com>
 \copyright 2017 ONEtastic s.r.l.
 

 \brief It is the parser main file.
 Definitions here is for how serial are reading or  writing to host.
*/

#include <stdlib.h>
#include <string.h>
#include "stm32f10x_map.h"

#include "dictionary.h"
#include "interpreter.h"
#include "terminal.h"
#include "fqueue.h"
#include "forth.h"
#include "cmd.h"


RQUEUE buffer;
char separators[]={' ', '\n', '\r', '\t', 0, -1 };
uint32_t write(void* device, char c);
uint32_t type(TERMINAL* term, char* str, uint32_t size);


void quit_loop(void *pvParameters)
{
    PROCESS *pid = (PROCESS*)pvParameters;
    INTERPRETER *interp = pid->interpreter;
    while(1){
        vTaskSuspend(pid->h_task);
        quit(interp);
    }
    
}


void query(TERMINAL* t)
{
    t->in = 0 ;
    memcpy(t->tib, t->queue->rdata, t->queue->rin);
    t->n_tib = t->queue->rin;
    t->queue->rin  = 0;
}

TERMINAL keyboard = {
    .device  = USART2,
    .queue   = &buffer,
    .base    = 10,
    .echo_en = 1,
    .write   = write,
    .type    = type,
    .query   = query,
};




const DICTIONARY commands[]={
#define X(NAME, PARAM, TOKEN) { NAME, PARAM, TOKEN },
    XMACRO_INSTRUCTIONS
    XAMP_STATUS
    XCMD
    XPSU_STATUS
    XOPTO_STATUS
    XRELAY_STATUS
#undef X
#define X(NAME, PARAM, TOKEN) {NAME, PARAM, TOKEN##_alarm},
    XAMP_ALARM
#undef X
#define X(NAME, PARAM, TOKEN) {NAME, PARAM, TOKEN##_alarm_config},
    XAMP_ALARM_CONFIG
#undef X

    {0,  0, 0}
};




INTERPRETER interpreter ={
    .dictionary = commands,
    .terminal   = &keyboard,
    .separators = separators,
    .stack   = {
        .push = push,
        .pull = pull,
        .get  = queue_get,
    }
};
