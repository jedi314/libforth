/** 
 \file serial-terminal.c
 \author: Giandomenico Rossi <g.rossi@onetastic.com>
 \copyright 2017 ONEtastic s.r.l.
 

 \brief 
 Contains serial terminal function  used by serial terminal and move it to 
 TI buffer to be interpreted.
 The serial terminal onwns its buffer containg all chars coming from low level device.
 Low level device has hardware dependent 

*/
#include <stdint.h>
#include "stm32f10x_lib.h"
#include "terminal.h"
#include "forth.h"
#include "fqueue.h"



#define USART_BANK GPIOA
#define RCC_APB2Periph_USART RCC_APB1Periph_USART2
#define RCC_APB2Periph_GPIO RCC_APB2Periph_GPIOA

#define USART_TX GPIO_Pin_2
#define USART_RX GPIO_Pin_3

#define USART_CLK   RCC_APB2Periph_AFIO   | \
                    RCC_APB2Periph_USART  | \
                    RCC_APB2Periph_GPIO

#define ENDL LF


typedef void (*init_sb)(void*);
extern PROCESS forth;



static void wait_until_char(USART_TypeDef* device)
{
    while(USART_GetFlagStatus(device, USART_FLAG_TC)!=SET) ;      
}

/**
\brief outputs char to the current UART.

\param term: pointer to SERIAL_TERMINAL struct
\param c: character to output
\return 0 if string is echoed 1 otherwise
*/
uint32_t write(void* device, char c)
{
    USART_TypeDef *usart = (USART_TypeDef*)device;
    
    wait_until_char(usart);
    USART_SendData(usart, c);

    return 0;
}


void USART2_IRQHandler(void)
{
    uint8_t c;
    USART_TypeDef *usart = (USART_TypeDef*)forth.interpreter->terminal->device;
    long xHigherPriorityTaskWoken = pdFALSE;
    RQUEUE *q = forth.interpreter->terminal->queue;

    if(USART_GetITStatus(usart, USART_IT_RXNE) == SET){
        c = USART_ReceiveData( usart );
        q->rdata[q->rin++] = c;
        if( c == ENDL){
            /* awake the task */
            vTaskResume(forth.h_task);
        }
    }
    portEND_SWITCHING_ISR( xHigherPriorityTaskWoken );
}   


void usart_init(void* device)
{
    USART_InitTypeDef usart_init;
    NVIC_InitTypeDef  nvic;
    GPIO_InitTypeDef  pin;   
    USART_TypeDef *usart = (USART_TypeDef*)device;

        
    /* pin configuration */
    RCC_APB1PeriphClockCmd(USART_CLK, ENABLE); /* Enable USART Clock */
    pin.GPIO_Pin   = USART_TX;
    pin.GPIO_Speed = GPIO_Speed_10MHz;
    pin.GPIO_Mode  = GPIO_Mode_AF_PP;
    GPIO_Init(USART_BANK, &pin);

    pin.GPIO_Pin   = USART_RX;
    pin.GPIO_Speed = GPIO_Speed_50MHz;
    pin.GPIO_Mode  = GPIO_Mode_IN_FLOATING;
    GPIO_Init(USART_BANK, &pin);

    /* USART */
    usart_init.USART_BaudRate   = 115200;
    usart_init.USART_WordLength = USART_WordLength_8b;
    usart_init.USART_StopBits   = USART_StopBits_1;
    usart_init.USART_Parity     = USART_Parity_No;
    usart_init.USART_HardwareFlowControl = USART_HardwareFlowControl_None;
    usart_init.USART_Mode       = USART_Mode_Rx | USART_Mode_Tx;
    USART_Init(usart, &usart_init);
    USART_ITConfig(usart, USART_IT_RXNE, ENABLE);

    /* Interrupt */
    nvic.NVIC_IRQChannel = USART2_IRQChannel;
    nvic.NVIC_IRQChannelPreemptionPriority = configLIBRARY_KERNEL_INTERRUPT_PRIORITY;
    nvic.NVIC_IRQChannelSubPriority = 0;
    nvic.NVIC_IRQChannelCmd = ENABLE;
    NVIC_Init(&nvic);

    /* enable and interrupt enable*/
    USART_Cmd(usart, ENABLE);
   
    
}


void com_port_config(init_sb init, void* dev )
{
    init(dev);
}
