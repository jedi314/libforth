/** 
 \file forth.h
 \author: Giandomenico Rossi <g.rossi@onetastic.com>
 \copyright 2017 ONEtastic s.r.l.
 

 \brief
*/

#ifndef _FORTH_H_
#define  _FORTH_H_

#include "interpreter.h"

#include "FreeRTOS.h"
#include "task.h"


#define USART2 ( USART_TypeDef * ) USART2_BASE

typedef struct {
    INTERPRETER *interpreter;
    xTaskHandle h_task;
    void (*loop)(void*);
}PROCESS;

typedef void (*init_sb)(void*);

void quit_loop(void *pvParameters);
void query(TERMINAL* t);
void com_port_config(init_sb init, void* dev );
void usart_init(void* device);


#endif
