/** 
 \file forth.h
 \author: Giandomenico Rossi <g.rossi@onetastic.com>
 \copyright 2017 ONEtastic s.r.l.
 

 \brief
*/

#ifndef _FORTH_H_
#define  _FORTH_H_

#include "interpreter.h"

void query(TERMINAL* t);

#endif
