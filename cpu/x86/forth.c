/** 
 \file forth.c
 \author: Giandomenico Rossi <g.rossi@onetastic.com>
 \copyright 2017 ONEtastic s.r.l.
 

 \brief x86 configurations
*/

#include <string.h>
#include <stdint.h>
#include <stdio.h>
#include "terminal.h"
#include "dictionary.h"
#include "interpreter.h"
#include "terminal.h"
#include "fqueue.h"

uint32_t print(void *dev, char c);
uint32_t type(TERMINAL* term, char* str, uint32_t size);

RQUEUE buffer;
char separators[]={' ', '\n', '\r', '\t', 0, -1 };

void q(TERMINAL* t)
{
    t->in = 0 ;
    memset(t->tib,0, TIB_SIZE);
    memcpy(t->tib, t->queue->rdata, t->queue->rin);
    t->n_tib = t->queue->rin;
    t->queue->rin  = 0;
}

TERMINAL keyboard = {
    .device  = 0,
    .queue   = &buffer,
    .base    = 10,
    .echo_en = 1,
    .write   = print,
    .type    = type,
    .query   = q,
};




const DICTIONARY commands[]={
#define X(NAME, PARAM, TOKEN) { NAME, PARAM, TOKEN },
    XMACRO_INSTRUCTIONS
#undef X
    {0,  0, 0}
};



INTERPRETER interpreter ={
    .dictionary = commands,
    .terminal   = &keyboard,
    .separators = separators,
    .stack = {
        .push = push,
        .pull = pull,
        .get  = queue_get,
    }
};

uint32_t print(void *dev, char c)
{
    printf("%c",c);
    return 0;
}

