/** 
 \file dictionary.c
 \author: Giandomenico Rossi <g.rossi@onetastic.com>
 \copyright 2017 ONEtastic s.r.l.
 

 \brief Dictionary list
*/
#include <stdio.h>
#include <stdlib.h>

//#include "main.h"

#include "dictionary.h"
#include "interpreter.h"
#include "fqueue.h"


uint32_t greet(QUEUE* q)
{
    INTERPRETER* interp = (INTERPRETER*)q;
    TERMINAL* term = interp->terminal;

    term->type(term, "HELLO!\n", -1);
    term->type(term, "CIAO!\n", -1);
    return 0;
}

uint32_t raw_dump_stack(QUEUE *q)
{
    INTERPRETER* interp = (INTERPRETER*)q;
    TERMINAL* term = interp->terminal;
    int32_t i;

    for(i=0; i<q->in; i++){
        type_number(term, q->data[i]);
        type_space(term);
    }
    return 0;
}



uint32_t dump_stack(QUEUE *q)
{
    INTERPRETER* interp = (INTERPRETER*)q;
    TERMINAL* term = interp->terminal;

    term->type(term, "[ ", -1);
    raw_dump_stack(q);
    term->type(term, "] ", -1);

    return 0;

}

uint32_t print_stack(QUEUE *q)
{
    raw_dump_stack(q);
    reset_queue(q);
    
    return 0;
}

uint32_t drop(QUEUE *q)
{
    q->in--;
    return 0;
}


uint32_t nip (QUEUE *q)
{
    int32_t a;

    a = q->pull(q);
    drop(q);
    q->push(q,a);

    return 0;
}


uint32_t plus(QUEUE *q)
{
    int32_t a, b;
    
    a=q->pull(q);
    b=q->pull(q);
    q->push(q, a+b);
    return 0;
}


uint32_t minus(QUEUE *q)
{
    int32_t a, b;
    
    a=q->pull(q);
    b=q->pull(q);
    q->push(q, a-b);
    return 0;
}


uint32_t star(QUEUE *q)
{
    int32_t a, b;
    
    a=q->pull(q);
    b=q->pull(q);
    q->push(q, a*b);
    return 0;
}


uint32_t slash_mod(QUEUE *q)
{
    int32_t a, b, c,d;
    
    b=q->pull(q);
    a=q->pull(q);
    c = a/b;
    d = a % b;
    q->push(q, d);
    q->push(q, c);
    return 0;
}


uint32_t mod(QUEUE *q)
{
    slash_mod(q);
    drop(q);
    return 0;
}


uint32_t slash(QUEUE *q)
{
    slash_mod(q);
    nip(q);
    return 0;
}


uint32_t star_slash_mode(QUEUE *q)
{
    int32_t a, b, c,d,e;
   
    c=q->pull(q);
    b=q->pull(q);
    a=q->pull(q);
    d = a*b/c;    /* TODO: parenthesis help to improve approximation ?  */
    e = a*b%c;    /* TODO: parenthesis help to improve approximation ? */
    q->push(q, e);
    q->push(q, d);

    return 0;
}


uint32_t star_slash(QUEUE *q)
{
    star_slash_mode(q);
    nip(q);

    return 0;
}


uint32_t dot(QUEUE *q)
{
    INTERPRETER* interp = (INTERPRETER*)q;
    TERMINAL* term = interp->terminal;
    int32_t a;

    a = q->pull(q);
    type_number(term, a);
    type_space(term);
    
    return 0;
}

#ifndef _BIT64_
uint32_t fetch(QUEUE *q)
{
    int32_t *p ;

    p = (int32_t*)(long)q->pull(q);
    q->push(q,*p);
    return 0;
}


uint32_t store(QUEUE *q)
{
    int32_t *p ;

    p = (int32_t*)(long)q->pull(q);
    *p = q->pull(q);
    return 0;
}

uint32_t cfetch(QUEUE *q)
{
    int8_t *p ;

    p = (int8_t*)(long)q->pull(q);
    q->push(q,*p);
    return 0;
}


uint32_t cstore(QUEUE *q)
{
    int8_t *p;

    p = (int8_t*)(long)q->pull(q);
    *p = (int8_t)q->pull(q);
    return 0;
}

#else
uint32_t fetch(QUEUE *q)
{
    int32_t *p ;
    uint64_t p64;

    p64 = (uint32_t)q->pull(q) | (uint64_t)q->pull(q) << 32;
    p = (int32_t*)p64;
    q->push(q,*p);
    return 0;
}


uint32_t store(QUEUE *q)
{
    int32_t *p;
    uint64_t p64;

    p64 = (uint32_t)q->pull(q) | (uint64_t)q->pull(q) << 32;
    p = (int32_t*)p64;
    *p = q->pull(q);
    return 0;
}

uint32_t cfetch(QUEUE *q)
{
    int8_t *p ;
    uint64_t p64;

    p64 = (uint32_t)q->pull(q) | (uint64_t)q->pull(q) << 32;
    p = (int8_t*)p64;
    q->push(q,*p);
    return 0;
}


uint32_t store(QUEUE *q)
{
    int32_t v, *p;
    uint64_t p64;

    p64 = (uint32_t)q->pull(q) | (uint64_t)q->pull(q) << 32;
    p = (int32_t*)p64;
    v = q->pull(q);
    *p = v ;
    return 0;
}
#endif

uint32_t udot(QUEUE *q)
{
    INTERPRETER* interp = (INTERPRETER*)q;
    TERMINAL* term = interp->terminal;
    
    term_printf(term, "%u ", q->get(q, q->in-1));
    return 0;
}

uint32_t depth(QUEUE *q)
{
    q->push(q, q->in);
    return 0;
}

uint32_t and(QUEUE *q)
{
    q->push(q, q->pull(q) & q->pull(q));
    return 0;
}


uint32_t or(QUEUE *q)
{
    q->push(q, q->pull(q) | q->pull(q));
    return 0;
}

uint32_t xor(QUEUE *q)
{
    q->push(q, q->pull(q) ^ q->pull(q));
    return 0;
}
uint32_t invert(QUEUE *q)
{
    q->push(q, ~q->pull(q));
    return 0;
}

uint32_t lshift(QUEUE *q)
{
    uint32_t s = q->pull(q);
    q->push(q, q->pull(q)<< s);
    return 0;
}


uint32_t rshift(QUEUE *q)
{
    uint32_t s = q->pull(q);
    q->push(q, q->pull(q) >> s);
    return 0;
}

uint32_t swap(QUEUE *q)
{
    int32_t a,b;
    a = q->pull(q);
    b = q->pull(q);
        
    q->push(q, a);
    q->push(q, b);
    return 0;

}

uint32_t dup(QUEUE *q)
{
    int32_t a;
    a = q->pull(q);
    
    q->push(q, a);
    q->push(q, a);
    return 0;

}


uint32_t pick(QUEUE *q)
{
    uint32_t n = q->pull(q);
    q->push(q, q->get(q, q->in-1-n));
    return 0;
}


uint32_t over(QUEUE *q)
{
    q->push(q, q->get(q, q->in-2));
    return 0;

}

uint32_t rot(QUEUE *q)
{
    int32_t a,b,c;
    a = q->pull(q);
    b = q->pull(q);
    c = q->pull(q);
    
    q->push(q, b);
    q->push(q, a);
    q->push(q, c);
    return 0;

}


static void dump_line(TERMINAL *term, uint8_t *p, uint32_t size)
{
    uint32_t i;
    char line[16];

    term_printf(term, "%p | ", p);
    for (i=0; i<size; i++){
        term_printf(term, "%02x ", *p);
        if (*p <32 || *p>126)
            line[i] = '.';
        else
            line[i] = *p;
        p++;
    }
    line[size] = 0;
    term_printf(term, "| %s\n", line);
}

uint32_t dump(QUEUE *q)
{
    INTERPRETER* interp = (INTERPRETER*)q;
    TERMINAL* term = interp->terminal;    
    uint32_t n,i;
    uint8_t *p ;

    n = (q->pull(q) + 15) / 16;
    p = (uint8_t*)(long)q->pull(q);

    for (i=0; i<n; i++){
        dump_line(term, p, 16);
        p += 16;
    }
    
    return 0;
}

uint32_t hex_dot(QUEUE *q)
{
    INTERPRETER* interp = (INTERPRETER*)q;
    TERMINAL* term = interp->terminal;    

    uint32_t base = term->base;
    term->base = 16;
    dot(q);
    term->base = base;

    return 0;
}

uint32_t tuck(QUEUE *q)
{
    int32_t first, second;

    first = q->pull(q);
    second = q->pull(q);
    q->push(q, first);
    q->push(q, second);
    q->push(q, first);
    return 0;
    
}

uint32_t question(QUEUE *q)
{
    fetch(q);
    dot(q);
    
    return 0;
    
}


uint32_t absolute(QUEUE *q)
{
    q->push(q, abs(q->pull(q)));
    
    return 0;
    
}

        
uint32_t maximum(QUEUE *q)
{
    int32_t a,b ;

    a = q->pull(q);
    b = q->pull(q);
    q->push(q, a>b?a:b );
    
    return 0;
    
}



        
uint32_t minimum(QUEUE *q)
{
    int32_t a,b ;

    a = q->pull(q);
    b = q->pull(q);
    q->push(q, a<b?a:b );
    
    return 0;
    
}

uint32_t negate(QUEUE *q)
{
    q->push(q, -q->pull(q));
    
    return 0;
    
}


uint32_t within(QUEUE *q)
{
    int32_t x, a, b, r;;
    b = q->pull(q);
    a = q->pull(q);
    x = q->pull(q);
    
    if(x>=a && x<b)
        r = -1;
    else
        r = 0;
    
    q->push(q,r);
    return 0;
    
}


uint32_t equal(QUEUE *q)
{
    int32_t  a, b;
    b = q->pull(q);
    a = q->pull(q);
    q->push(q, a == b ? -1 : 0);
    return 0;
    
}


uint32_t not(QUEUE *q)
{
    q->push(q, !q->pull(q));
    return 0;
    
}


uint32_t uless(QUEUE *q)
{
    uint32_t a, b;
    b = (uint32_t)q->pull(q);
    a = (uint32_t)q->pull(q);
    q->push(q, a<b?-1:0);

    return 0;
}


uint32_t umore(QUEUE *q)
{
    uint32_t a, b;
    b = (uint32_t)q->pull(q);
    a = (uint32_t)q->pull(q);
    q->push(q, a>b?-1:0);

    return 0;
}


uint32_t less(QUEUE *q)
{
    int32_t a, b;
    b = q->pull(q);
    a = q->pull(q);
    q->push(q, a<b?-1:0);

    return 0;
}


uint32_t more(QUEUE *q)
{
    int32_t a, b;
    b = q->pull(q);
    a = q->pull(q);
    q->push(q, a>b?-1:0);

    return 0;
}


uint32_t not_equal(QUEUE *q)
{
    int32_t a, b;
    b = q->pull(q);
    a = q->pull(q);
    q->push(q, a!=b?-1:0);
    return 0;
}

uint32_t zero_equal(QUEUE *q)
{
    int32_t a;
    a = q->pull(q);
    q->push(q, a==0?-1:0);
    return 0;
}


uint32_t zero_not_equal(QUEUE *q)
{
    int32_t a;
    a = q->pull(q);
    q->push(q, a!=0?-1:0);
    return 0;
}


uint32_t zero_gt(QUEUE *q)
{
    int32_t a;
    a = q->pull(q);
    q->push(q, a>0?-1:0);
    return 0;
}
