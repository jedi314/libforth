/** ModularorT2
 \file interpreter.h
 \author: Giandomenico Rossi <g.rossi@onetastic.com>
 \copyright 2017 ONEtastic s.r.l.
 

 \brief
 The DICTIONARY structure contains all implemented commands
 The INTERPRETER has its onwn DICTIONARY used to search word 
 get from TIB 

 The interpreter has a
 The interpreter owns a dictionary containig all base words ( command )
 to be executed.
 It appempts to execute or convert to a number each word found
 in the current input buffer.  
 The token is returned by word and the following algorithm is applied:
 
 1. Search a token inside the dictionary.
 2. If found execute it.
 3. If not found, chek if it is a number then put it on stack
 4. Else return Error
 
*/


#ifndef _INTERPRETR_H
#define _INTERPRETR_H

#include <stdint.h>
#include "terminal.h"
#include "dictionary.h"
#include "throw.h"

typedef struct{
    QUEUE stack;
    DICTIONARY* dictionary;
    TERMINAL* terminal;
    char* separators;
    jmp_buf t;
}INTERPRETER;

typedef enum
{
    TOKEN_FOUND     = 0,
    TOKEN_LEN_ZERO  = 1,
    TOKEN_NOT_FOUND = 6,
    NUMBER_PUSH     = 7,
}INTERPRETER_ERRORS;


void quit(INTERPRETER *interp);

#endif
