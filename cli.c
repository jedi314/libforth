/* gcc -lreadline cli.c -o cli */

#include <stdio.h>

#include <readline.h>
#include <history.h>
#include <stdlib.h>

#include "interpreter.h"
#include "x86/forth.h"

extern INTERPRETER interpreter;

int main()
{
    char *line;
    uint32_t size;
    int a = 2;
    printf("%p %ld %ld %ld", &a, sizeof(uint32_t *), sizeof(int), sizeof(uint32_t));
    while ( 1 ) {
        line = readline("ONE> ");
        add_history(line);
        size = strlen(line);
        strcpy(interpreter.terminal->queue->rdata, line);
        interpreter.terminal->queue->rin = size;
        quit(&interpreter);
        free(line);
    }

    return 0;

}
