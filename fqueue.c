/** 
 \file queue.c
 \author: Giandomenico Rossi <g.rossi@onetastic.com>
 \copyright 2017 ONEtastic s.r.l.
 

 \brief
*/
#include "fqueue.h"
#include <stdio.h>

/**
\brief  returns true if there's input 
ready in the receive queue.

\param q: pointer to RQUEUE struct
\return returns true if there's input 
ready in the receive queue.
*/
uint32_t new_key(RQUEUE* q)
{
    return q->rin != q->rout;
}

/**
\brief reads the next character from the queue. 
\param q: pointer to RQUEUE struct
\return 
*/
uint8_t key(RQUEUE* q)
{
    uint8_t c; 
    c = q->rdata[q->rout++];
    q->rout &= 0xff;             /* mod 256 */

    return c;
}

void reset_rqueue(RQUEUE* q)
{
    q->rin  = 0;
    q->rout = 0;
}



/**
\brief store the next character from the rqueue. 
\param q: pointer to RQUEUE struct
\param v: value to store
\return QUEUE_OK or QUEUE_OVERFLOW
*/
uint32_t rqueue_push(RQUEUE* q, char v )
{
    if (q->rin >= QUEUE_SIZE)
        return QUEUE_OVERFLOW;
    
    q->rdata[q->rin++] = v ;
    return QUEUE_OK;
}





/*  */

uint32_t queue_full(QUEUE* q)
{
    return q->in >= QUEUE_SIZE;
}


uint32_t queue_empty(QUEUE* q)
{
    return q->in < 0;
}


/**
\brief reads the next character from the 32bit queue. 
Need to use queue_empty before get.
\param q: pointer to QUEUE struct
\return data.
*/
int32_t pull(QUEUE* q)
{
    int32_t c;
    c = q->data[--q->in];
    return c;
}


/**
\brief store the next character from the 32bit queue. 
\param q: pointer to QUEUE struct
\param v: value to store
\return QUEUE_OK or QUEUE_OVERFLOW
*/
uint32_t push(QUEUE* q, int32_t v )
{
    if (q->in >= QUEUE_SIZE)
        return QUEUE_OVERFLOW;
    
    q->data[q->in++] = v ;
    return QUEUE_OK;
}

uint32_t queue_get(QUEUE* q, uint32_t n)
{
    return q->data[n];
}

uint32_t get_last(QUEUE* q)
{
    return q->data[q->in-1];
}

void reset_queue(QUEUE* q)
{
    q->in = 0;
}
