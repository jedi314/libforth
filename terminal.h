/** 
 \file serial-terminal.h
 \author: Giandomenico Rossi <g.rossi@onetastic.com>
 \copyright 2017 ONEtastic s.r.l.
 

 \brief
 Strutcure of serial ring QUEUE used to store received chars.

 The  ring QUEUE contains data coming from serial and two index, rin and rout.
 rin contains teh current position of charcter read from serial/terminal while
 rout contains buffer read from serial termial task.

 An interrupt routine or other, takes the arriving character and put it in the 
 QUEUE, incrementing rin modulo QUEUE size.

 A consumer reads data from routine and increment rout modulo QUEUE size.

 Raw chars are stored to ring queue by ISR or general other input handler.
*/

#ifndef _SERIAL_TERMINAL_H
#define _SERIAL_TERMINAL_H

#include <stdint.h>
#include <stdarg.h>

#include "fqueue.h"


#define TIB_SIZE 128

typedef struct TERMINAL{
    void* device;               /**< Pointer to device structure */
    RQUEUE* queue;              /**< Pointer to serial ring queue  */
    uint16_t base;              /**< number base */
    uint16_t echo_en;           /**< echo or not incoming chars  */
    uint16_t in;                /**< index or parsed chars  */
    uint16_t n_tib;             /**< number of char copied to tib */
    char tib[TIB_SIZE];         /**< Terminal Input Buffer  */

    /* Device dependent function pointers */
    void (*init)(void*);
    uint32_t (*write)(void*, char);
    uint32_t (*type)(struct TERMINAL*, char*, uint32_t);
    void (*query)(struct TERMINAL*);
}TERMINAL;

uint32_t emit(TERMINAL* term, char c);
uint32_t type(TERMINAL* term, char* str, uint32_t size);
uint32_t accept(TERMINAL* term);


void type_error(TERMINAL* term, char* mag, uint32_t size);
void type_ok(TERMINAL* term);
void type_cr(TERMINAL* term);
void type_lf(TERMINAL* term);
uint32_t type_number(TERMINAL* term, int32_t n);
void type_space(TERMINAL* term);
uint32_t term_printf(TERMINAL* term, char* format, ... );



#define BS  8
#define LF  10
#define CR  13
#define DEL 127
#define BL  32


#endif
