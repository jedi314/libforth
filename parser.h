/** 
 \file parser.h
 \author: Giandomenico Rossi <g.rossi@onetastic.com>
 \copyright 2017 ONEtastic s.r.l.
 

 \brief Parser Exported Functions
*/

#ifndef _PARSER_H_
#define _PARSER_H_

#include "parser.h"
#include "interpreter.h"

uint32_t word(TERMINAL* term, char* sep, char** addr);
void query(TERMINAL* term);

#endif
